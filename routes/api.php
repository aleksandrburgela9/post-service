<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'Api'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('login', 'LoginController@login')->name('login');
        Route::post('logout', 'LoginController@logout');
        Route::post('register', 'RegisterController@register');
    });
});


Route::prefix('/user')->group(function () {
    Route::middleware('auth:api')->group(function () {
        Route::get('/', 'Api\UserController@getUser');
        Route::post('/', 'Api\UserController@create');
        Route::get('/{id}', 'Api\UserController@getUserById');
        Route::post('/{id}', 'Api\UserController@update');
        Route::delete('/', 'Api\UserController@delete');
    });
});

Route::prefix('/post')->group(function () {
    Route::middleware('auth:api')->group(function () {
        Route::get('/', 'Api\PostController@all');
        Route::post('/', 'Api\PostController@create');
        Route::get('/{id}', 'Api\PostController@read');
        Route::post('/{id}', 'Api\PostController@update');
        Route::delete('/{id}', 'Api\PostController@delete');
    });
});

