<?php

use App\Model\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new User)->create([
           'name' => 'admin',
           'email' => 'admin@example.com',
           'password' => Hash::make('admin')
        ]);
    }
}
