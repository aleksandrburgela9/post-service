<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;

use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function create(Request $request)
    {
        $errors = $this->validator($request->all())->getMessageBag();

        if ($errors->getMessages()) {
            return response()->json(['errors' => $errors->getMessageBag()]);
        }

        return response()->json(
            User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ])
        );

    }

    public function getUser(Request $request)
    {
        return response()->json($request->user());
    }

    public function getUserById(Request $request, $id)
    {
        return response()->json(User::find($id));
    }

    public function update(Request $request, $id)
    {
        $errors = $this->validatorUpdate($request->all())->getMessageBag();

        if ($errors->getMessages()) {
            return response()->json(['errors' => $errors->getMessageBag()]);
        }

        $user = User::find($id);
        if ($user) {
            $user->update([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]);

            return response()->json($user);
        }

        return response()->json(['message' => 'User not found']);
    }

    public function delete(Request $request)
    {
        $request->user()->delete();
        return response()->json(['message' => 'User has been delete']);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    protected function validatorUpdate(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

}
