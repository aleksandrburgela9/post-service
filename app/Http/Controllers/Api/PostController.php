<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Model\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function create(Request $request)
    {
        $errors = $this->validator($request->all())->getMessageBag();

        if ($errors->getMessages()) {
            return response()->json(['errors' => $errors->getMessageBag()]);
        }

        return response()->json(
            $request->user()->posts()->create([
                'title' => $request['title'],
                'content' => $request['content']
            ])
        );

    }

    public function all(Request $request)
    {
        return response()->json($request->user()->posts()->get());
    }

    public function read(Request $request, $id)
    {
        return response()->json($request->user()->posts()->where('id', $id)->first());
    }

    public function update(Request $request, $id)
    {
        $errors = $this->validator($request->all())->getMessageBag();

        if ($errors->getMessages()) {
            return response()->json(['errors' => $errors->getMessageBag()]);
        }

        $post = $request->user()->posts()->where('id', $id)->first();

        if ($post) {
            $post->update([
                'title' => $request['title'],
                'content' => $request['content']
            ]);

            return response()->json($post);
        }

        return response()->json(['message' => 'Post not found']);
    }

    public function delete(Request $request, $id)
    {
        $post = $request->user()->posts()->where('id', $id)->first();
        if ($post) {
            $post->delete();
            return response()->json(['message' => 'Post has been delete']);
        }
        return response()->json(['message' => 'Post not found']);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string', 'min:10'],
        ]);
    }
}
