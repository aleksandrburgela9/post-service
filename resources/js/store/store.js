import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'http://localhost:8000/api/'

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null,
    },
    getters: {
        loggedIn(state) {
            return state.token !== null
        }
    },
    mutations: {
        retrieveToken(state, token) {
            state.token = token
        },
        destroyToken(state) {
            state.token = null
        }
    },
    actions: {
        deleteUser(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.delete('user/').then(res => {
                    resolve(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        updateUser(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.post('user/' + data.id, {
                    title: data.title,
                    email: data.email,
                    name: data.name,
                    password: data.password
                }).then(res => {
                    if (res.data.id != null) {
                        resolve(res)
                    }
                    reject(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        getUser(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.get('user/').then(res => {
                    console.log(res)
                    if (res.data.id != null) {
                        resolve(res)
                    }
                    reject(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        createUser(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.post('user/', {
                    email: data.email,
                    name: data.name,
                    password: data.password
                }).then(res => {
                    if (res.data.errors == null) {
                        resolve(res)
                    }
                    reject(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        getPosts(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.get('post/').then(res => {
                    if (res.data != null) {
                        resolve(res)
                    }
                    reject(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        updatePost(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.post('post/' + data.id, {
                    title: data.title,
                    content: data.content,
                }).then(res => {
                    if (res.data.id != null) {
                        resolve(res)
                    }
                    reject(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        getPost(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.get('post/' + data.id).then(res => {
                    if (res.data.id != null) {
                        resolve(res)
                    }
                    reject(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        deletePost(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.delete('post/' + data.id).then(res => {
                    resolve(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        createPost(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.post('post', {
                    title: data.title,
                    content: data.content,
                }).then(res => {
                    if (res.data.id != null) {
                        resolve(res)
                    }
                    reject(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        register(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('register', {
                    email: data.email,
                    name: data.name,
                    password: data.password,
                    password_confirmation: data.password_confirmation
                }).then(res => {
                    if (res.data.message != null) {
                        resolve(res)
                    }
                    reject(res)
                }).catch(err => {
                    reject(err)
                })
            })
        },
        destroyToken(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.post('logout',).then(res => {
                        localStorage.removeItem('access_token')
                        context.commit('destroyToken')
                        resolve(res)
                    }).catch(err => {
                        localStorage.removeItem('access_token')
                        context.commit('destroyToken')
                        reject(err);
                    })
                })
            }
        },
        retrieveToken(context, credential) {
            return new Promise((resolve, reject) => {
                axios.post('login', {
                    email: credential.email,
                    password: credential.password,
                }).then(res => {
                    console.log(res)
                    const token = res.data.access_token

                    localStorage.setItem('access_token', token)
                    context.commit('retrieveToken', token)
                    resolve(res)
                }).catch(err => {
                    console.log(err);
                    reject(err);
                })
            })
        }
    }
})
