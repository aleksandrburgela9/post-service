import Vue from 'vue';

//require('./bootstrap');

window.Vue = require('vue');

import {store} from './store/store'
import router from "./router";
import Master from './layout/Master'

import VueNotifications from 'vue-notification'


Vue.use(VueNotifications)


const app = new Vue({
    store,
    router,
    components: {Master},
    el: '#app'
});
