import Vue from 'vue';
import VueRouter from 'vue-router';
import Posts from "./components/Posts";
import Login from "./components/auth/Login";
import Logout from "./components/auth/Logout";
import Register from "./components/auth/Register";
import AddPost from "./components/post/AddPost";
import DeletePost from "./components/post/DeletePost";
import EditPost from "./components/post/EditPost";
import ViewPost from "./components/post/ViewPost";
import AddUser from "./components/user/AddUser";
import EditUser from "./components/user/EditUser";
import DeleteUser from "./components/user/DeleteUser";

Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {path: '/', name: 'posts', component: Posts},
        {path: '/login', name: 'login', component: Login, meta: {requiresVisitor: true}},
        {path: '/logout', name: 'logout', component: Logout, meta: {requiresVisitor: true}},
        {path: '/register', name: 'register', component: Register, meta: {requiresVisitor: true}},
        {path: '/post/add', name: 'postAdd', component: AddPost, meta: {requiresVisitor: true}},
        {path: '/post/delete/:id', name: 'postDelete', component: DeletePost, meta: {requiresVisitor: true}},
        {path: '/post/edit/:id', name: 'postEdit', component: EditPost, meta: {requiresVisitor: true}},
        {path: '/post/:id', name: 'postView', component: ViewPost, meta: {requiresVisitor: true}},
        {path: '/user/add', name: 'userAdd', component: AddUser, meta: {requiresVisitor: true}},
        {path: '/user/edit', name: 'userEdit', component: EditUser, meta: {requiresVisitor: true}},
        {path: '/user/delete', name: 'userDelete', component: DeleteUser, meta: {requiresVisitor: true}}
    ],
    mode: 'history'
})
